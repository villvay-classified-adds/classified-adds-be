<?php

namespace App\Http\Controllers\Api;

use App\Advertisement;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;

class AdvertisementController extends Controller
{
     public $error = "error";

    protected function validator(Request $request)
    {
        return $this->validate($request, [
            'title' => 'required|string|max:255',
            'description' => 'required|min:1|max:65535',
            'is_fav' => 'required|integer|min:0|max:1',
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $this->validator($request);

            // store image.
            $advertisement_img = $request->advertisement_img;
            $image = $advertisement_img["encoded"]; // base64 encoded
            $imageName = time() . '_' . str_random(10) . '.' . $advertisement_img["file_ext"];

            Storage::disk('images')->put($imageName, base64_decode($image));

            $model = new Advertisement();
            $model->title = $request->title;
            $model->description = $request->description;
            $model->category_id = $request->category_id;
            $model->img_path = $imageName;
            $model->is_fav = $request->is_fav;
            $model->created_at = Carbon::now();
            $model->updated_at = Carbon::now();

            $saved = $model->save();
            $advertisement="";
            if($saved){
                $advertisement = Advertisement::with(['category'])
                ->where(['id' => $model->id])
                ->first();
            }
            return response()->json(Helper::formatResponse($saved, $advertisement), 200);

        } catch (\Exception $e) {
            return response()->json(Helper::formatResponse(false, [$this->error => $e->getMessage()]), 200);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdvertisements(Request $request)
    {
        try {
            $advertisements = Advertisement::with(
                [
                    'category'
                ]
            )
                ->get()
                ->toArray();

            return response()->json(Helper::formatResponse(true, $advertisements), 200);
        } catch (\Exception $e) {
            return response()->json(Helper::formatResponse(false, [$this->error => $e->getMessage()]), 200);
        }        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAdvertisementById(Request $request)
    {
        try {
            $advertisement = Advertisement::with(
                [
                    'category'
                ]
            )
                ->where(['id' => $request->id])
                ->first();

            return response()->json(Helper::formatResponse(true, $advertisement), 200);
        } catch (\Exception $e) {
            return response()->json(Helper::formatResponse(false, [$this->error => $e->getMessage()]), 200);
        }        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function toggleFav(Request $request)
    {
        if ($request->has('is_fav')) {
            $updateData["is_fav"] = $request->is_fav;
        }

        $updated = Advertisement::where('id', $request->id)->update($updateData);
        if ($updated) {
            $model = Advertisement::findOrFail($request->id);
            return response()->json(Helper::formatResponse(true, $model), 200);
        }
        return response()->json(Helper::formatResponse(false, ""), 200);
        
    }
}
