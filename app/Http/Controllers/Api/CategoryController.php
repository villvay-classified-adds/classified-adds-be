<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;

class CategoryController extends Controller
{
    public $error = "error";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getAll(Request $request)
    {
        try {
            $modelRows = Category::all();

            return response()->json(Helper::formatResponse(true, $modelRows), 200);
        } catch (\Exception $e) {
            return response()->json(Helper::formatResponse(false, [$this->error => $e->getMessage()]), 200);
        }
    }
    
    
    
}
