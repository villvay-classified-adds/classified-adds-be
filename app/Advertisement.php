<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement extends Model
{
     protected $table = 'advertisement';

     public function category()
    {
        return $this->belongsTo('App\Category');
    }
}
