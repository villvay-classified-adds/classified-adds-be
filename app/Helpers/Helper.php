<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

class Helper
{
    public static function formatResponse($type, $content)
    {
        return ['status' => $type, "content" => $content];
    }
}
