<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/category/get-all', 'Api\CategoryController@getAll');
Route::get('/advertisement/get-all', 'Api\AdvertisementController@getAdvertisements');
Route::get('/advertisement/get-by-id', 'Api\AdvertisementController@getAdvertisementById');
Route::post('/advertisement/create', 'Api\AdvertisementController@store');
Route::put('/advertisement/toggleFav', 'Api\AdvertisementController@toggleFav');
