<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class Category extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 255 );
            $table->timestamps();
        });

        // Insert metadata
        $categories = [
            ['name'=> 'Community', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name'=> 'Professional', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name'=> 'Housing', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name'=> 'Buying', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name'=> 'Selling', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name'=> 'Jobs', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name'=> 'Education', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()],
            ['name'=> 'Commercial', 'created_at' => Carbon::now(), 'updated_at' => Carbon::now()]
        ];
        DB::table('category')->insert($categories);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category');
    }
}
